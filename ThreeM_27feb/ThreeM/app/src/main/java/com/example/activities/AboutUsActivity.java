package com.example.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.threem.R;


public class AboutUsActivity extends AppCompatActivity implements View.OnClickListener {


    private WebView webView;
    private ImageView imgBack;
    private TextView txtActivityName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);

        init();
        txtActivityName.setText(getResources().getString(R.string.about_us));
        imgBack.setOnClickListener(this);
    }



    private void init() {
        webView = findViewById(R.id.webView);
        imgBack = findViewById(R.id.imgBack);
        txtActivityName = findViewById(R.id.txtActivityName);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://techsumitsolutions.com/");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imgBack:
                finish();
                break;

            default:
             break;
        }


    }
}