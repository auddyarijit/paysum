package com.example.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.threem.R;

public class ProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtName,txtFullName,txtEmail;
    private TextView txtBussinessName,txtCountry,txtLandMark;
    private TextView txtllRegDate;
    private AppCompatButton btnLogout;
    private ImageView imgBack;
    private TextView txtActivityName;

    String name,bussinessName,fullName,country,landmark,regDate,email;
    Integer bussinessId,userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        init();
        clickEvents();
        getData();
        setData();
    }

    private void clickEvents() {
        btnLogout.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    private void setData() {
        txtName.setText(name);
        txtFullName.setText(fullName);
        txtEmail.setText(email);
        txtBussinessName.setText(bussinessName);
        txtLandMark.setText(landmark);
        txtllRegDate.setText(regDate);
        txtCountry.setText(country);
        txtActivityName.setText("Profile");
    }

    //data is coming from login activity
    private void getData() {
        Bundle bundle = getIntent().getExtras();

        if (bundle!=null)
        {
            name = bundle.getString("name");
            bussinessName = bundle.getString("bussinessName");
            fullName = bundle.getString("fullName");
            country = bundle.getString("country");
            landmark = bundle.getString("landmark");
            regDate = bundle.getString("regDate");
            bussinessId = bundle.getInt("bussinessId");
            userId = bundle.getInt("userId");
            email = bundle.getString("email");
        }

    }

    private void init() {
        txtName =findViewById(R.id.txtName);
        txtFullName =findViewById(R.id.txtFullName);
        txtEmail =findViewById(R.id.txtEmail);
        txtBussinessName =findViewById(R.id.txtBussinessName);
        txtCountry =findViewById(R.id.txtCountry);
        txtLandMark =findViewById(R.id.txtLandMark);
        txtllRegDate =findViewById(R.id.txtllRegDate);
        btnLogout =findViewById(R.id.btnLogout);
        imgBack =findViewById(R.id.imgBack);
        txtActivityName =findViewById(R.id.txtActivityName);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imgBack:
                finish();
                break;

            case R.id.btnLogout:
                Intent logout = new Intent(ProfileActivity.this,LoginActivity.class);
                logout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(logout);
                break;
        }
    }
}