package com.example.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.threem.R;
import com.google.zxing.WriterException;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class QRCodeActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgQr;
    private ImageView imgBack;
    private TextView txtActivityName;


    Bitmap bitmap;
    QRGEncoder qrgEncoder;

    String invoiceNo,customerName,invoiceDate,areaName;

    KeyGenerator keyGenerator;

    SecretKey secretKey;
    byte[] IV = new byte[16];
    SecureRandom random;

    String encryptInvoice,encryptCust,encryptDate,encryptareaName;

    byte [] data = "�¢DË\f;z�97\u0016�:U�<<7�W��\u001Ch�j�4� 0\t�\u0005++R\u0019%\f\u000Fz@������]ۇ\u0010�|d�\f־е".getBytes();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_q_r_code);

        init();
        clickEvents();
        getData();

        enc();


        try {
            decrypt(data, secretKey, IV);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void enc() {
        try {
            keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(256);
            secretKey = keyGenerator.generateKey();
            String key = secretKey.toString();

            Toast.makeText(this, "" +key, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        random = new SecureRandom();
        random.nextBytes(IV);

        String qrdata = invoiceNo+"-"+customerName+"-"+invoiceDate+"-"+areaName;

        try {
            byte[]  encryptInv = encrypt(qrdata.getBytes(), secretKey, IV);

            encryptInvoice = new String(encryptInv, "UTF-8");

            //call method for creating qr
             generateQr();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static byte[] encrypt(byte[] plaintext, SecretKey key, byte[] IV) throws Exception {
        Cipher cipher = Cipher.getInstance("AES");
        SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(IV);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);
        byte[] cipherText = cipher.doFinal(plaintext);

        String cip = cipherText.toString();
        String keyy = key.toString();

        String decValue = decrypt(cipherText,key,IV);

        return cipherText;
    }



    public static String decrypt(byte[] cipherText, SecretKey key, byte[] IV) throws Exception {
        try {
            Cipher cipher = Cipher.getInstance("AES");
            SecretKeySpec keySpec = new SecretKeySpec(key.getEncoded(), "AES");
            IvParameterSpec ivSpec = new IvParameterSpec(IV);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            byte[] decryptedText = cipher.doFinal(cipherText);
            return new String(decryptedText);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }




    private void clickEvents() {
        imgBack.setOnClickListener(this);
    }

    private void init() {
        imgQr = findViewById(R.id.imgQr);
        imgBack = findViewById(R.id.imgBack);
        txtActivityName = findViewById(R.id.txtActivityName);

        txtActivityName.setText("QR");
    }

    //data is coming from invoice history list
    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null)
        {
            invoiceNo = bundle.getString("invoiceNo");
            customerName = bundle.getString("customerName");
            invoiceDate = bundle.getString("invoiceDate");
            areaName = bundle.getString("areaName");
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.imgBack:
                finish();
                break;

        }

    }




    private void generateQr() {
        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
        // initializing a variable for default display.
        Display display = manager.getDefaultDisplay();

        // creating a variable for point which
        // is to be displayed in QR Code.
        Point point = new Point();
        display.getSize(point);

        // getting width and
        // height of a point
        int width = point.x;
        int height = point.y;

        // generating dimension from width and height.
        int dimen = width < height ? width : height;
        dimen = dimen * 3 / 4;

        // setting this dimensions inside our qr code
        // encoder to generate our qr code.
        // invoiceNo+"\n"+customerName+"\n "+invoiceDate +"\n"+areaName
        qrgEncoder = new QRGEncoder(encryptInvoice, null, QRGContents.Type.TEXT, dimen);
        try {
            // getting our qrcode in the form of bitmap.
            bitmap = qrgEncoder.encodeAsBitmap();
            // the bitmap is set inside our image
            // view using .setimagebitmap method.
            imgQr.setImageBitmap(bitmap);
        } catch (WriterException e) {
            // this method is called for
            // exception handling.
            Log.e("Tag", e.toString());
        }
    }


}