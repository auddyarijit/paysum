package com.example.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.model.LoginResponse;
import com.example.remote.RetrofitClient;
import com.example.threem.R;

import retrofit2.Call;


public class UserFragment extends Fragment {

    public UserFragment() { }

    private TextView txtName,txtFullName,txtEmail;
    private TextView txtBussinessName,txtLocation,txtLandMark;
    private TextView txtllRegDate;

    String name,bussinessName,fullName,country,landmark,regDate;;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user, container, false);


        txtName =rootView.findViewById(R.id.txtName);
        txtFullName =rootView.findViewById(R.id.txtFullName);
        txtEmail =rootView.findViewById(R.id.txtEmail);
        txtBussinessName =rootView.findViewById(R.id.txtBussinessName);
        txtLocation =rootView.findViewById(R.id.txtLocation);
        txtLandMark =rootView.findViewById(R.id.txtLandMark);
        txtllRegDate =rootView.findViewById(R.id.txtllRegDate);



        name = getArguments().getString("name");
        bussinessName = getArguments().getString("bussinessName");
        fullName = getArguments().getString("fullName");
        name = getArguments().getString("name");


        return rootView;
    }




}