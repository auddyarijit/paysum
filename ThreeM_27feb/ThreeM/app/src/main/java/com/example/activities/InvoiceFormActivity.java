package com.example.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.app.ActivityCompat;

import com.example.model.DemoPojo;
import com.example.remote.RetrofitClient;
import com.example.threem.R;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoiceFormActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    private ImageView imgFront,imgBack,imgSideOne,imgSideTwo;
    public  static final int RequestPermissionCode  = 1 ;

    private AppCompatButton btnSubmit;
    ArrayList<String> imageList = new ArrayList<>();

    //private Spinner spinner;

    Bitmap bitmap;
    Button selectImg,uploadImg;
    private  static final int IMAGE = 100;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_form);
       // getSupportActionBar().hide();

        EnableRuntimePermission();

        imgFront = findViewById(R.id.imgFront);
        imgBack = findViewById(R.id.imgBack);
        imgSideOne = findViewById(R.id.imgSideOne);
        imgSideTwo = findViewById(R.id.imgSideTwo);
        btnSubmit = findViewById(R.id.btnSubmit);

        //spinner = findViewById(R.id.spinner);

        imgFront.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        imgSideOne.setOnClickListener(this);
        imgSideTwo.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

       // spinner.setOnClickListener(this);

        //dropDownSpinner();
    }


    private String convertToString(Bitmap bitmap)
    {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream);
        byte[] imageBytes = byteArrayOutputStream.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }




    private void uploadImage(){

        Call<DemoPojo> call = RetrofitClient
                .getInstance()
                .getApi()
                .uploadImage(imageList);


        call.enqueue(new Callback<DemoPojo>() {
            @Override
            public void onResponse(Call<DemoPojo> call, Response<DemoPojo> response) {

                Log.d("Server Response","".toString());
                if (response.isSuccessful())
                {
                    startActivity(new Intent(InvoiceFormActivity.this, CertificateActivity.class));
                }
            }

            @Override
            public void onFailure(Call<DemoPojo> call, Throwable t) {
                Log.d("Server Response",""+t.toString());
            }
        });

    }








    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgFront:
                captureImage();
                break;

            case R.id.imgBack:
                captureImageBack();
                break;


            case R.id.imgSideOne:
                captureSideOne();
                break;

            case R.id.imgSideTwo:
                captureimgSideTwo();
                break;

            case R.id.btnSubmit:
                uploadImage();
                //startActivity(new Intent(InvoiceFormActivity.this, InvoiceActivity.class));
                break;

        }
    }

    private void captureImage() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 1);
    }

    private void captureImageBack() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 2);
    }

    private void captureSideOne() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 3);
    }

    private void captureimgSideTwo() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, 4);
    }




    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK && data!=null) {
            Bitmap  bitmap= (Bitmap) data.getExtras().get("data");
                 imgFront.setImageBitmap(bitmap);
                 String one =  convertToString(bitmap);
                 imageList.add(one);
        }
       else if (requestCode == 2 && resultCode == RESULT_OK) {
            Bitmap bitmapBack = (Bitmap) data.getExtras().get("data");
            imgBack.setImageBitmap(bitmapBack);
            String two =   convertToString(bitmapBack);
            imageList.add(two);
           // imageList.add(String.valueOf(image));
        }

        else if (requestCode == 3 && resultCode == RESULT_OK) {
            Bitmap bitmapSideOne = (Bitmap) data.getExtras().get("data");
            imgSideOne.setImageBitmap(bitmapSideOne);
            String three =   convertToString(bitmapSideOne);
            imageList.add(three);
           // imageList.add(String.valueOf(image));
        }

        else if (requestCode == 4 && resultCode == RESULT_OK) {
            Bitmap bitmapSideTwo = (Bitmap) data.getExtras().get("data");
            imgSideTwo.setImageBitmap(bitmapSideTwo);
            String four =   convertToString(bitmapSideTwo);
            imageList.add(four);
            //imageList.add(String.valueOf(image));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(InvoiceFormActivity.this,
                Manifest.permission.CAMERA))
        {
            Toast.makeText(InvoiceFormActivity.this,"CAMERA permission allows us to Access CAMERA app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(InvoiceFormActivity.this,new String[]{
                    Manifest.permission.CAMERA}, RequestPermissionCode);
        }

    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] PResult) {

        switch (RC) {

            case RequestPermissionCode:

                if (PResult.length > 0 && PResult[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(InvoiceFormActivity.this,"Permission Granted", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(InvoiceFormActivity.this,"Permission Canceled", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }





    //unuse code
    private void dropDownSpinner() {
        ArrayList<String> vechicleNameList = new ArrayList<>();
        vechicleNameList.add("Ashok Leyland");
        vechicleNameList.add("Eicher Motors");
        vechicleNameList.add("Tata Motors");
        vechicleNameList.add("Mahindra & Mahindra");
        vechicleNameList.add("Force Motors");
        vechicleNameList.add("Asia Motor Works");
        vechicleNameList.add("Volvo Trucks");
        vechicleNameList.add("Bharat Benz");


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, vechicleNameList);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        //spinner.setAdapter(adapter);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item =  parent.getItemAtPosition(position).toString();
        Toast.makeText(this, "" +item, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }



}