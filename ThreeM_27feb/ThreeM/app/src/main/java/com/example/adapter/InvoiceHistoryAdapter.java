package com.example.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.activities.QRCodeActivity;
import com.example.model.InvoiceHistoryResponseData;
import com.example.threem.R;
import java.util.List;


public class InvoiceHistoryAdapter extends RecyclerView.Adapter<InvoiceHistoryAdapter.InvoiceHistoryViewHolder> {

    private Activity activity;
    private List<InvoiceHistoryResponseData> historyDataList;

    public class InvoiceHistoryViewHolder extends RecyclerView.ViewHolder {

        public TextView txtName,txtInvoiceNo,txtDate;

        public InvoiceHistoryViewHolder(View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txtName);
            txtInvoiceNo = itemView.findViewById(R.id.txtInvoiceNo);
            txtDate = itemView.findViewById(R.id.txtDate);
        }
    }


    public InvoiceHistoryAdapter(Activity activity, List<InvoiceHistoryResponseData> historyDataList) {
        this.activity = activity;
        this.historyDataList = historyDataList;
    }


    @Override
    public InvoiceHistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.invoice_history, parent, false);
        return new InvoiceHistoryViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(InvoiceHistoryViewHolder holder, int position) {
        InvoiceHistoryResponseData dataList = historyDataList.get(position);

        String customerName = dataList.getCustomerName();
        String invoiceDate = dataList.getInvoiceDate();
        String areaName = dataList.getAreaName();

        holder.txtName.setText(dataList.getCustomerName());
        holder.txtInvoiceNo.setText(String.valueOf(dataList.getInvoiceId()));
        holder.txtDate.setText(dataList.getInvoiceDate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String invoiceNo = String.valueOf(dataList.getInvoiceId());
                Intent intent = new Intent(activity, QRCodeActivity.class);
                intent.putExtra("invoiceNo",invoiceNo);
                intent.putExtra("customerName",customerName);
                intent.putExtra("invoiceDate",invoiceDate);
                intent.putExtra("areaName",areaName);
                activity.startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return historyDataList.size();
    }


}

