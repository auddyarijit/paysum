package com.techsum.paysum.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.pnsol.sdk.interfaces.PaymentTransactionConstants;
import com.pnsol.sdk.payment.PaymentInitialization;
import com.pnsol.sdk.vo.DeviceInformation;
import com.pnsol.sdk.vo.response.ICCTransactionResponse;
import com.techsum.paysum.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/*
 * This class is used to process the payment transaction. From the
 * SharedPreferences gets the Bluetooth MAC address. If the MAC address is
 * presents within SharedPreferences then it connects the BluetoothConnection
 * class and establish the BluetoothSocket between the PED and application to
 * process the payment transaction. Otherwise, if MAC address is not presents in
 * SharedPreferences then displays dialog box
 */

public class PaymentTransactionActivity extends Activity implements
        PaymentTransactionConstants {

    public static final int REQUEST_ENABLE_BT = 1;
    private static String DEVICE_COMMUNICATION_MODE = "transactionmode";
    private static String DEVICE_NAME = "devicename";
    private static String MAC_ADDRESS = "macAddress";
    private static String DEVICE_TYPE = "devicetype";
    private BluetoothAdapter mBtAdapter;
    private boolean bluetoothOnFlag, checkFlag;
    private String cashBackAmount = null;
    private String merchantRefNo, deviceName, paymentoptioncode, deviceMACAddress, paymentType;
    private ListView emvList;
    private int deviceCommMode;
    private PaymentInitialization initialization;
    private com.pnsol.sdk.vo.request.EMI emivo;
    //private int deviceType;

    private String roll_no, userId, school_id, student_id, school_name, date;

    private int paidFee = 0;
    private int dueFee = 0;
    private int grandAmount = 0;
    private int transAmount =0;

    private String amount, name;
    private Map<String, Bitmap> map;

    private ArrayList<String> titleList;
    private ArrayList<String> amountList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_transaction);
        deviceCommMode = getIntent().getIntExtra(DEVICE_COMMUNICATION_MODE, 0);
        deviceMACAddress = getIntent().getStringExtra(MAC_ADDRESS);
        paymentType = getIntent().getStringExtra(PAYMENT_TYPE);
        deviceName = getIntent().getStringExtra(DEVICE_NAME);
        amount = getIntent().getStringExtra("amount");


        merchantRefNo = getIntent().getStringExtra("referanceno");


        getData();


        if (merchantRefNo == null || merchantRefNo.equalsIgnoreCase("null") || merchantRefNo.equalsIgnoreCase("")) {
            merchantRefNo = String.valueOf(System.currentTimeMillis());
        }

        if (paymentType.equalsIgnoreCase(EMI)) {
            emivo = (com.pnsol.sdk.vo.request.EMI) getIntent().getSerializableExtra("vo");
            paymentoptioncode = getIntent().getStringExtra("paymentcode");
        }
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();
        //Toast.makeText(this, "" + mBtAdapter, Toast.LENGTH_SHORT).show();

    }



    private void getData() {

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            paidFee = bundle.getInt("paidFee");
            dueFee = bundle.getInt("dueFee");
            grandAmount = bundle.getInt("grandAmount");
            amount = bundle.getString("amount");

            transAmount = bundle.getInt("transAmount");


            school_id = bundle.getString("school_id");
            roll_no = bundle.getString("roll_no");
            school_name = bundle.getString("school_name");
            userId = bundle.getString("userId");
            name = bundle.getString("name");


            titleList = (ArrayList<String>) getIntent().getSerializableExtra("titleList");
            amountList = (ArrayList<String>) getIntent().getSerializableExtra("amountList");


           // Toast.makeText(this, "" + bundle, Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bluetooth is enabled or not if not then asks for enable
        if (!checkFlag) {
            if (mBtAdapter != null) {
                mBtAdapter.cancelDiscovery();
                if (!mBtAdapter.isEnabled()) {
                    bluetoothOnFlag = false;
                    Intent enableIntent = new Intent(
                            BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                } else {
                    bluetoothOnFlag = true;
                }
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!checkFlag) {
            if (bluetoothOnFlag) {
                initiateConnection();
            }
        }
    }

    private void initiateConnection() {
        isDeviceON(getString(R.string.pls_chk_cardreader_availble));
    }


    public void deviceConnection(String address) {
        if (paymentType.equalsIgnoreCase(APP_UPDATE)) {
            initialization = new PaymentInitialization(PaymentTransactionActivity.this);
            initialization.appUpdate(handler, this, deviceName);
        } else {
            try {
                initialization = new PaymentInitialization(
                        PaymentTransactionActivity.this);

                initialization.initiateTransaction(handler, deviceName, address, amount, paymentType, "POS",
                        null, null, 71.000001, 17.000001, merchantRefNo, null, deviceCommMode, merchantRefNo, "", "");
            } catch (RuntimeException e) {
                e.printStackTrace();
            }
        }
    }


    // The Handler that gets information back from the PaymentProcessThread
    @SuppressLint("HandlerLeak")
    private final Handler handler = new Handler() {

        public void handleMessage(android.os.Message msg) {
            checkFlag = true;
            if (msg.what == SOCKET_NOT_CONNECTED) {

                if (!isFinishing() && !isDestroyed() ) {
                    alertMessage((String) msg.obj);
                }

               // alertMessage((String) msg.obj);
            }
            if (msg.what == QPOS_ID) {
                Toast.makeText(PaymentTransactionActivity.this, (String) msg.obj, Toast.LENGTH_LONG).show();
                finish();

            } else if (msg.what == DEVICE_INFO) {
                DeviceInformation deviceInfo = (DeviceInformation) msg.obj;
                Toast.makeText(PaymentTransactionActivity.this, "Device Serail number: " + deviceInfo.getSerialNumer() + "\n" + "Model number:" + deviceInfo.getModelNumber(), Toast.LENGTH_LONG).show();
                finish();
            } else if (msg.what == CHIP_TRANSACTION_APPROVED
                    || msg.what == SWIP_TRANSACTION_APPROVED) {
                ICCTransactionResponse iCCTransactionResponse = (ICCTransactionResponse) msg.obj;


                Intent intent = new Intent(PaymentTransactionActivity.this,
                        TransactionDetailsActivity.class);

                intent.putExtra("vo", iCCTransactionResponse);

                intent.putExtra("paymentType", paymentType);


                //mpaysdk 2.0
                intent.putExtra("grandAmount", grandAmount);
                intent.putExtra("dueFee", dueFee);
                intent.putExtra("paidFee", paidFee);

                intent.putExtra("roll_no", roll_no);
                intent.putExtra("userId", userId);
                intent.putExtra("school_id", school_id);
                intent.putExtra("school_name", school_name);
                intent.putExtra("name", name);
                intent.putExtra("amount", amount);

                intent.putExtra("titleList", (Serializable) titleList);
                intent.putExtra("amountList", (Serializable) amountList);


                finish();
                PaymentTransactionActivity.this.startActivity(intent);

                Toast.makeText(PaymentTransactionActivity.this, "Transaction Completed", Toast.LENGTH_LONG).show();

            } else if (msg.what == CHIP_TRANSACTION_DECLINED
                    || msg.what == SWIP_TRANSACTION_DECLINED) {
                ICCTransactionResponse vo = (ICCTransactionResponse) msg.obj;
                Intent i = new Intent(PaymentTransactionActivity.this,
                        TransactionDetailsActivity.class);

                i.putExtra("vo", vo);
                i.putExtra("paymentType", paymentType);
                PaymentTransactionActivity.this.startActivity(i);
                Toast.makeText(PaymentTransactionActivity.this, "Transaction Status : " + vo.getResponseCode() + ":" + vo.getResponseMessage(), Toast.LENGTH_LONG).show();
                finish();
            } else if (msg.what == QPOS_DEVICE) {

                if (!isFinishing() && !isDestroyed() ) {
                    alertMessage((String) msg.obj);
                }


               // alertMessage((String) msg.obj);
            } else if (msg.what == TRANSACTION_FAILED) {
                ICCTransactionResponse vo = null;
                if (msg.obj instanceof ICCTransactionResponse) {
                    vo = (ICCTransactionResponse) msg.obj;
                }
                Toast.makeText(PaymentTransactionActivity.this, "Transaction Status : " + vo.getResponseCode() + ":" + vo.getResponseMessage(), Toast.LENGTH_LONG).show();

                if (paymentType.equalsIgnoreCase(EMI)) {
                    Intent i = new Intent(PaymentTransactionActivity.this, TransactionDetailsActivity.class);
                    i.putExtra("vo", vo);
                    i.putExtra("paymentType", paymentType);
                    PaymentTransactionActivity.this.startActivity(i);
                    Toast.makeText(PaymentTransactionActivity.this, "Transaction Status : " + vo.getResponseCode() + ":" + vo.getResponseMessage(), Toast.LENGTH_LONG).show();
                    finish();
                } else {

                    finish();
                }
            } else if (msg.what == TRANSACTION_INITIATED) {
                Toast.makeText(PaymentTransactionActivity.this, msg.obj.toString(), Toast.LENGTH_LONG).show();

            } else if (msg.what == ERROR_MESSAGE) {

                if (msg.obj instanceof ICCTransactionResponse) {
                    ICCTransactionResponse vo = (ICCTransactionResponse) msg.obj;
                    Toast.makeText(PaymentTransactionActivity.this, vo.getResponseCode() + ":" + vo.getResponseMessage(), Toast.LENGTH_LONG).show();
                    finish();
                } else {
                    if (!isFinishing() && !isDestroyed() ) {
                        alertMessage((String) msg.obj);
                    }
                    // alertMessage((String) msg.obj);
                }
            } else if (msg.what == TRANSACTION_PENDING) {
                Toast.makeText(PaymentTransactionActivity.this,
                        (String) msg.obj + "Pending status", Toast.LENGTH_SHORT).show();
                finish();
            } else if (msg.what == DISPLAY_STATUS) {

                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.custom_toast,
                        (ViewGroup) findViewById(R.id.toast_layout_root));


                Toast toast = new Toast(getApplicationContext());
                toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();

              /*  Toast.makeText(PaymentTransactionActivity.this,
                        (String) msg.obj, Toast.LENGTH_SHORT).show();*/
            } else if (msg.what == QPOS_EMV_MULITPLE_APPLICATION) {
                ArrayList<String> applicationList = (ArrayList<String>) msg.obj;
                emvList = (ListView) findViewById(R.id.application_list);
                emvList.setVisibility(View.VISIBLE);
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(PaymentTransactionActivity.this, android.R.layout.simple_list_item_1, applicationList);
                emvList.setAdapter(adapter);
                emvList.setOnItemClickListener(new OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        if (initialization != null) {
                            initialization.getQposListener().executeSelectedEMVApplication(position);
                            emvList.setVisibility(View.GONE);
                        }
                    }
                });
            } else if (msg.what == SUCCESS) {

                Toast.makeText(PaymentTransactionActivity.this,
                        (String) msg.obj, Toast.LENGTH_SHORT).show();
                finish();
            }

        }


    };

    public void alertMessage(String message) {

        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Alert").setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        finish();
                    }
                }).show();

    }

    public void isDeviceON(String message) {
        deviceConnection(deviceMACAddress);
    }
}
