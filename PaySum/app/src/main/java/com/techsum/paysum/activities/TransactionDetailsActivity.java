package com.techsum.paysum.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pnsol.sdk.interfaces.DeviceType;
import com.pnsol.sdk.interfaces.PaymentTransactionConstants;
import com.pnsol.sdk.interfaces.ReceiptConst;
import com.pnsol.sdk.payment.PaymentInitialization;
import com.pnsol.sdk.vo.response.ICCTransactionResponse;
import com.pnsol.sdk.vo.response.TransactionStatusResponse;
import com.techsum.paysum.R;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static android.view.PixelCopy.SUCCESS;

public class TransactionDetailsActivity extends AppCompatActivity implements View.OnClickListener, PaymentTransactionConstants {

    private TextView txtPrint;
    private TextView txtName,txtRollNo,txtTotalFee,txtDate,txtTime;
    private TextView txtSchool,txtFeeDue,txtFeePaid,txtTransactionId;

    private ImageView imgBack;
    private TextView txtTitle;

    private String paymentMode;
    private ICCTransactionResponse iccTransactionResponse;

    private String roll_no, userId, school_id, time, school_name, date;

    private int paidFee = 0;
    private int dueFee = 0;
    private int grandAmount = 0;

    private String amount, name;
    private Calendar calendar;
    private SimpleDateFormat dateFormat;

    private PaymentInitialization initialization;
    private Bitmap bitmap;


    private ArrayList<String> titleList;
    private ArrayList<String> amountList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_details);
        getSupportActionBar().hide();


        init();
        clickEvents();
        getData();
        setData();



        iccTransactionResponse = (ICCTransactionResponse) getIntent()
                .getSerializableExtra("vo");
        if (iccTransactionResponse != null) {
            String data = "";
            try {

                data = new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(iccTransactionResponse);
             //   Toast.makeText(this, ""+data, Toast.LENGTH_SHORT).show();
            } catch (JsonGenerationException e1) {
                e1.printStackTrace();
            } catch (JsonMappingException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }


        getCurrentDate();
        getCurrentTime();

    }

    private void init() {
        txtPrint = findViewById(R.id.txtPrint);
        txtName = findViewById(R.id.txtName);
        txtRollNo = findViewById(R.id.txtRollNo);
        txtTotalFee = findViewById(R.id.txtTotalFee);
        txtDate = findViewById(R.id.txtDate);
        txtTime = findViewById(R.id.txtTime);
        txtSchool = findViewById(R.id.txtSchool);
        txtFeeDue = findViewById(R.id.txtFeeDue);
        txtFeePaid = findViewById(R.id.txtFeePaid);
        txtTransactionId = findViewById(R.id.txtTransactionId);
        imgBack = findViewById(R.id.imgBack);
        txtTitle = findViewById(R.id.txtTitle);
    }



    private void getData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            paidFee = bundle.getInt("paidFee");
            dueFee = bundle.getInt("dueFee");
            grandAmount = bundle.getInt("grandAmount");
            amount = bundle.getString("amount");

            school_id = bundle.getString("school_id");
            roll_no = bundle.getString("roll_no");
            school_name = bundle.getString("school_name");
            userId = bundle.getString("userId");
            name = bundle.getString("name");

            titleList = (ArrayList<String>) getIntent().getSerializableExtra("titleList");
            amountList = (ArrayList<String>) getIntent().getSerializableExtra("amountList");

            paymentMode = getIntent().getStringExtra(PAYMENT_TYPE);
        }
    }


    private void setData() {
        txtName.setText(name);
        txtFeePaid.setText(String.valueOf(amount));
        txtFeeDue.setText(String.valueOf(dueFee));
        txtTotalFee.setText(String.valueOf(grandAmount));

        txtRollNo.setText(roll_no);
        txtSchool.setText(school_name);
         //txtTransactionId.setText(iccTransactionResponse.transactionStatus);
    }


    private void getCurrentDate() {
        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        date = dateFormat.format(calendar.getTime());
        txtDate.setText(date);
    }

    public void getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("hh:mm:ss aa");
        time = mdformat.format(calendar.getTime());
        txtTime.setText(time);
    }


    private void clickEvents() {
        txtPrint.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txtPrint:
                printLogo(ReceiptConst.CENTER);
                printText();
                break;

            case R.id.imgBack:
                finish();
                break;

                default:
                break;
        }
    }


    private void printText() {
        StringBuffer scriptBuffer = new StringBuffer();

        scriptBuffer.append("*text l " + "  " + " " + "Name :" + " " + name + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Roll No :" + " " + roll_no + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Transction id :" + " " + "2885385745" + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Date :" + " " + date + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Time :" + " " + time + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "School :" + " " + school_name + "\n");

        for (int i = 0; i < titleList.size(); i++) {
            scriptBuffer.append("*text l " + "  " + " " + titleList.get(i) + " - " + " " + amountList.get(i) + "\n");
        }

        scriptBuffer.append("*line" + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Amount:" + " " + grandAmount + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Fee Paid:" + " " + paidFee + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Fee Due:" + " " + dueFee + "\n");
        scriptBuffer.append("*text l " + "" + " " + " " + "\n");
        scriptBuffer.append("*text l " + "" + " " + " " + "\n");
        scriptBuffer.append("*text l " + "" + " " + " " + "\n");
        scriptBuffer.append("*text l " + "" + " " + " " + "\n");
        scriptBuffer.append("*text l " + "" + " " + " " + "\n");

        initialization = new PaymentInitialization(TransactionDetailsActivity.this);
        initialization.printText(printerHandler, DeviceType.N910, scriptBuffer);
        paymentSuccessDialog();
    }



    @SuppressLint("HandlerLeak")
    private final Handler printerHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == SUCCESS) {
                Toast.makeText(TransactionDetailsActivity.this,
                        (String) msg.obj, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(TransactionDetailsActivity.this,
                        (String) msg.obj, Toast.LENGTH_SHORT).show();
            }
        }
    };



    private void paymentSuccessDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_dailog_payment_success);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setTitle("Payment Type");
        dialog.setCancelable(false);

        Button btnOk = dialog.findViewById(R.id.btnOk);
        Button btnCopyReciept = dialog.findViewById(R.id.btnCopyReciept);

        //TextView for cashPaymentActivity
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TransactionDetailsActivity.this, DashBoardActivity.class);
                startActivity(intent);
                // startActivity(new Intent(StudentDetailsActivity.this,CashPaymentActivity.class));
                dialog.dismiss();
            }
        });


        btnCopyReciept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printLogo(ReceiptConst.CENTER);
                printText();
                Intent intent = new Intent(TransactionDetailsActivity.this, DashBoardActivity.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });


        dialog.show();
    }


    private void printLogo(String alignment) {
        bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.paysum_logo);
        initialization = new PaymentInitialization(TransactionDetailsActivity.this);
        initialization.printImage(printerHandler, DeviceType.N910, bitmap, alignment);
    }

    private void getDetails() {
        try {
            PaymentInitialization initialization = new PaymentInitialization(TransactionDetailsActivity.this);
            initialization.initiateTransactionDetails(handler, iccTransactionResponse.getReferenceNumber(), null,paymentMode , null, null);
            Toast.makeText(this, ""+initialization, Toast.LENGTH_SHORT).show();
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("HandlerLeak")
    private final Handler handler = new Handler() {

        public void handleMessage(android.os.Message msg) {
            if (msg.what == SUCCESS) {
                //
                List<TransactionStatusResponse> transactionStatusResponse = (List<TransactionStatusResponse>) msg.obj;

                for(int i=0;i>transactionStatusResponse.size();i++){
                    if(TRANSACTION_APPROVED.equalsIgnoreCase(transactionStatusResponse.get(i).getTransactionStatus())){
                        Log.d("transaction","APPROVE");
                    }else {
                        Log.d("transaction","failed");
                    }
                }

               /* Intent i = new Intent(TransactionDetailsActivity.this, PaymentDetails.class);
                i.putExtra("txnResponse", transactionStatusResponse.get(0));

                startActivity(i);
*/
            }
            if (msg.what == FAIL) {
                Toast.makeText(TransactionDetailsActivity.this, (String) msg.obj,
                        Toast.LENGTH_LONG).show();
                //finish();
            } else if (msg.what == ERROR_MESSAGE) {
                Toast.makeText(TransactionDetailsActivity.this, (String) msg.obj,
                        Toast.LENGTH_LONG).show();
            }

        }

        ;
    };
}