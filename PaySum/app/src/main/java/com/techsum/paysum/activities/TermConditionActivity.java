package com.techsum.paysum.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.techsum.paysum.R;

public class TermConditionActivity extends AppCompatActivity implements View.OnClickListener {

    private  TextView txtLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_condition);
        getSupportActionBar().hide();

        init();
        clickListener();

        //for hindi translator action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));
    }


    private void init() {
        txtLogin = findViewById(R.id.txtLogin);
    }


    private void clickListener() {
        txtLogin.setOnClickListener(this);
    }



    @Override
    public void onClick(View view)
    {
        switch (view.getId()) {
            case R.id.txtLogin:
                startActivity(new Intent(TermConditionActivity.this,LoginActivity.class));
                break;

            default:
                break;
        }
    }


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface arg0, int arg1) {
                        TermConditionActivity.super.onBackPressed();
                    }
                }).create().show();
    }




}