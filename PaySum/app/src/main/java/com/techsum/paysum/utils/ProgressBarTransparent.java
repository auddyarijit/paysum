package com.techsum.paysum.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;

import com.techsum.paysum.R;

public class ProgressBarTransparent  {
    public static ProgressBarTransparent mCShowProgress;
    public Dialog mDialog;

    public ProgressBarTransparent() {
    }

    public static ProgressBarTransparent getInstance() {
        if (mCShowProgress== null) {
            mCShowProgress= new ProgressBarTransparent();
        }
        return mCShowProgress;
    }

    public void show(Context mContext) {
        mDialog= new Dialog(mContext);
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        mDialog.setContentView(R.layout.custom_progress_layout);
        mDialog.findViewById(R.id.progress_bar).setVisibility(View.VISIBLE);
        mDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        mDialog.setCancelable(true);
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();
    }

    public void hideProgress() {
        if (mDialog!= null) {
            mDialog.dismiss();
            mDialog= null;
        }
    }
}

