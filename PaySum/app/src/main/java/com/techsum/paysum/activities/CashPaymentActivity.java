package com.techsum.paysum.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.techsum.paysum.R;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by sanjay on 04/12/2020
 * module cashPayment
 */

public class CashPaymentActivity extends AppCompatActivity implements View.OnClickListener , AdapterView.OnItemSelectedListener{


    private TextView txtCashPayment,txtChargeRemaining;
    private TextView txtActivityName,txtTotalDue;
    private Spinner spinnerFeePlan;
    private EditText edtCash;
    private ImageView imgBack,imgLogout;

    String userId,student_id,roll_no,school_id,school_name;
    String name;

    private int grandAmount = 0;

    private int paidFee =0;
    private int dueFee =0;
    private int transAmount =0;
    private int finalTotal =0;

    String inputText = "";

    ArrayList<String> feeArrayList = new ArrayList<>();

    //list for subject wise fee
    ArrayList<String> amountList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_payment);
        getSupportActionBar().hide();

        //custome toolbar and set activity name on TextView(txtActivityName)
        txtActivityName = findViewById(R.id.txtActivityName);
        txtActivityName.setText(getString(R.string.cash_payment));

        //intializing textview,image and other
        init();
        //click event listner
        clicklistener();

        makerArrayList();
        calcutionPart();

        imgLogout = findViewById(R.id.imgLogout);
        imgLogout.setVisibility(View.GONE);

        //for hindi translator action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));

        getData();
        setSpinnerMakerAdapter();

        txtTotalDue.setText(String.valueOf(grandAmount));

    }

    private void makerArrayList() {
        feeArrayList.add("Monthly");
        feeArrayList.add("Quarterly");
        feeArrayList.add("Half-yearly");
        feeArrayList.add("Yearly");
    }


    private void getData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getString("userId");
            student_id = extras.getString("student_id");
            roll_no = extras.getString("roll_no");
            grandAmount = extras.getInt("grandAmount");
            name = extras.getString("name");
            school_name = extras.getString("school_name");

            ArrayList<String> titleList = (ArrayList<String>) getIntent().getSerializableExtra("titleList");
            amountList = (ArrayList<String>) getIntent().getSerializableExtra("amountList");

            school_id = extras.getString("school_id");
        }
    }


    private void calcutionPart() {
        edtCash.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {   //Convert the Text to String
                inputText = edtCash.getText().toString();

                if (inputText.length()>0) {
                    paidFee = Integer.parseInt(inputText);
                    Log.d("price", String.valueOf(paidFee));
                    dueFee = (grandAmount - paidFee);
                    txtChargeRemaining.setText(Integer.toString(dueFee));
                }
                else
                {
                    txtChargeRemaining.setText(Integer.toString(grandAmount));
                }

                txtTotalDue.setText(String.valueOf(grandAmount));

                // for invoice transcation amount deduction
                transCalculation();


                finalTotal = (paidFee + transAmount);

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Does not do any thing in this case
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Does not do any thing in this case
            }
        });
    }

    private void transCalculation() {
        if (paidFee>2000)
        {
            transAmount = 30;
            return;
        }
        else if(paidFee>=1000)
        {
            transAmount =20;
            return;
        }
        else if (paidFee<=1000)
        {
            transAmount = 10;
            return;
        }

    }

    private void validation() {

        String fee = edtCash.getText().toString();
        if (fee.isEmpty()) {
            edtCash.setError(getString(R.string.payment_amount));
            edtCash.requestFocus();
            return;
        }

        else {
            Intent intent = new Intent(CashPaymentActivity.this, PaymentConfirmedActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("grandAmount",grandAmount);

            ArrayList<String> titleList = (ArrayList<String>) getIntent().getSerializableExtra("titleList");
            intent.putExtra("titleList", (Serializable) titleList);
            intent.putExtra("amountList", (Serializable) amountList);

            intent.putExtra("school_name", school_name);
            intent.putExtra("roll_no", roll_no);
            intent.putExtra("name", name);

            intent.putExtra("paidFee", paidFee);
            intent.putExtra("dueFee", dueFee);


          // Toast.makeText(this, ""+ finalTotal, Toast.LENGTH_SHORT).show();

            startActivity(intent);

        }
    }


    private void init() {
        txtCashPayment = findViewById(R.id.txtCashPayment);
        imgBack  = findViewById(R.id.imgBack);
        edtCash = findViewById(R.id.edtCash);
        txtChargeRemaining = findViewById(R.id.txtChargeRemaining);
        txtTotalDue  = findViewById(R.id.txtTotalDue);
        spinnerFeePlan  = findViewById(R.id.spinnerFeePlan);
    }

    private void clicklistener() {
        txtCashPayment.setOnClickListener(this);
        imgBack.setOnClickListener(this);
        spinnerFeePlan.setOnItemSelectedListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtCashPayment:
                validation();
                //startActivity(new Intent(CashPaymentActivity.this,PaymentConfirmedActivity.class));
                break;

            case R.id.imgBack:
                finish();
                break;
        }
    }


    private void setSpinnerMakerAdapter() {
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,feeArrayList);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFeePlan.setAdapter(aa);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}