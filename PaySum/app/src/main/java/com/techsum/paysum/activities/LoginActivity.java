package com.techsum.paysum.activities;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.techsum.paysum.R;
import com.techsum.paysum.model.LoginResponse;
import com.techsum.paysum.network.RetrofitClient;
import com.techsum.paysum.utils.ProgressBarTransparent;

import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sanjay on 04/12/2020
 * module Login
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView txtLogin;
    private EditText edtUserName,edtPassword;
    private TextView txtActivityName;
    private ImageView imgLangauge;

    SharedPreferences sharedpreferences;

    String school_id,userId;


    //private AVLoadingIndicatorView progressBar;

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String Name = "name";
    public static final String Phone = "password";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        txtActivityName = findViewById(R.id.txtActivityName);
        txtActivityName.setText(getString(R.string.login));

        //intializing textview,image and other
        init();

        //click event listner
        clickListener();

        //for hindi translator action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

    }


    private void clickListener() {
        txtLogin.setOnClickListener(this);
        imgLangauge.setOnClickListener(this);
    }

    private void init() {
        txtLogin = findViewById(R.id.txtLogin);
        edtUserName = findViewById(R.id.edtUserName);
        edtPassword = findViewById(R.id.edtPassword);
        imgLangauge = findViewById(R.id.imgLangauge);
     //   progressBar = findViewById(R.id.progressBar);
    }


    @Override
    public void onClick(View view)
    {
        switch (view.getId()) {
            case R.id.txtLogin:
                 validation();
                 hideKeyboard();
                 break;

            case R.id.imgLangauge:
                langaugeDailog();
                break;
        }
    }


    private void hideKeyboard()
    {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
    }

    private void validation() {

        String username = edtUserName.getText().toString();
        String userpassword = edtPassword.getText().toString();


        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(Name, username);
        editor.putString(Phone, userpassword);
        editor.commit();


        if(username.length()==0){
            edtUserName.setError(getString(R.string.username_error));
            edtUserName.requestFocus();
            return;
        }

        else if(userpassword.length()==0){
            edtPassword.setError(getString(R.string.password_error));
            edtPassword.requestFocus();
            return;
        }

        else if(userpassword.length()<3){
            edtPassword.setError(getString(R.string.pass_minimum_length));
            edtPassword.requestFocus();
            return;
        }



        //for transparent activity progress bar
        ProgressBarTransparent progressBar = ProgressBarTransparent.getInstance();
        progressBar.show(LoginActivity.this);


       // progressBar.show();

        Call<LoginResponse> call = RetrofitClient
                .getInstance()
                .getApi()
                .loginResponse(username,userpassword);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

               //boolean successful = response.isSuccessful();
                 userId = response.body().getId();
                school_id = response.body().getSchoolId();

                if (userId!=null)
                {
                    loginSuccesToast();

                    userId = response.body().getId();
                    school_id  = response.body().getSchoolId();

                    Intent intent = new Intent(LoginActivity.this, DashBoardActivity.class);
                    intent.putExtra("userId", userId);
                    intent.putExtra("school_id", school_id);
                    startActivity(intent);
                    progressBar.hideProgress();

                    edtUserName.setText("");
                    edtPassword.setText("");
                }
                else {
                    loginFailedToast();
                    progressBar.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                 //simpleProgressBar.setVisibility(View.GONE);
                progressBar.hideProgress();
                Toast.makeText(LoginActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void loginFailedToast() {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Incorrect credential", Snackbar.LENGTH_LONG);
        snackbar.setDuration(3000);
        snackbar.setBackgroundTint(Color.RED);
        snackbar.show();
    }

    private void loginSuccesToast() {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Login Succesfully", Snackbar.LENGTH_LONG);
        snackbar.setDuration(3000);
        snackbar.setBackgroundTint(getResources().getColor(R.color.app_primary_color));
        snackbar.show();
    }


    private void langaugeDailog() {

        final  String[]  listView= {"English","हिंदी"};

        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("select Langauge");
        builder.setSingleChoiceItems(listView, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {

                if (i == 0)
                {
                    //english
                    setLocal("en");
                    recreate();
                }

                else if (i == 1)
                {
                    //hindi
                    setLocal("hi");
                    recreate();
                }

                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void setLocal(String lang) {
        Locale locale = new Locale(lang);
        locale.setDefault(locale);
        Configuration configuration = new Configuration();
        configuration.locale =locale;
        getBaseContext().getResources().updateConfiguration(configuration,getBaseContext().getResources().getDisplayMetrics());
    }





}