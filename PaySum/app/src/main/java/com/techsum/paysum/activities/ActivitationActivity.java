package com.techsum.paysum.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pnsol.sdk.auth.AccountValidator;
import com.pnsol.sdk.interfaces.PaymentTransactionConstants;
import com.pnsol.sdk.vo.response.LoginResponse;
import com.techsum.paysum.R;

public class ActivitationActivity extends AppCompatActivity implements PaymentTransactionConstants , View.OnClickListener {


    private EditText edtPartnerKey,edtMerchantKey;
    private TextView txtActivate;
    private TextView txtActivityName;
    private ImageView imgLangauge;

    private static final int MY_PERMISSIONS_REQUEST_WRITE_CALENDAR = 123;
    private String partnerAPIKey = "2016665A802D";
    private Activity activity;

    private static final int REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;
    private int writeExternalStoragePermission;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activitation);
        getSupportActionBar().hide();

        activity = ActivitationActivity.this;

        init();



        edtPartnerKey.setText("");
        edtMerchantKey.setText("");

        txtActivate.setOnClickListener(this);

        imgLangauge.setVisibility(View.GONE);
    }

    private void init() {
        edtPartnerKey= findViewById(R.id.edtPartnerKey);
        edtMerchantKey= findViewById(R.id.edtMerchantKey);
        txtActivate= findViewById(R.id.txtActivate);
        txtActivityName= findViewById(R.id.txtActivityName);
        imgLangauge= findViewById(R.id.imgLangauge);

        txtActivityName.setText("Activitation");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txtActivate:
                activate();
                break;

               default:
                break;
        }
    }

    private void activate() {

        String mkey = edtPartnerKey.getText().toString();
        String pkey = edtMerchantKey.getText().toString();
        if (mkey != null && pkey != null) {

            try {
                AccountValidator validator = new AccountValidator(
                        getApplicationContext());

                validator.accountActivation(handler, mkey, pkey);

            } catch (RuntimeException e) {
                e.printStackTrace();
            }

        } else {
            Toast.makeText(ActivitationActivity.this, getString(R.string.enter_key),
                    Toast.LENGTH_SHORT).show();
        }
    }


    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == SUCCESS) {
                LoginResponse vo = (LoginResponse) msg.obj;

                Toast.makeText(ActivitationActivity.this, "" + vo.getResponseMessage(), Toast.LENGTH_LONG).show();
                Intent i = new Intent(ActivitationActivity.this,
                        SplashActivity.class);
                startActivity(i);
            }

            if (msg.what == FAIL) {
                Toast.makeText(ActivitationActivity.this, (String) msg.obj,
                        Toast.LENGTH_SHORT).show();
            } else if (msg.what == ERROR_MESSAGE) {
                Toast.makeText(ActivitationActivity.this, (String) msg.obj,
                        Toast.LENGTH_LONG).show();
            }
        }

    };




    @Override
    protected void onResume() {
        super.onResume();
        AccountValidator validator = new AccountValidator(getApplicationContext());
        if (validator.isAccountActivated()) {
            Intent i = new Intent(ActivitationActivity.this,
                    SplashActivity.class);
            startActivity(i);
            finish();
        }
    }

        @Override
        public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            if(requestCode==REQUEST_CODE_WRITE_EXTERNAL_STORAGE_PERMISSION){
                int grantResultsLength = grantResults.length;
                if(grantResultsLength > 0 && grantResults[0]== PackageManager.PERMISSION_GRANTED)
                {

                }else
                {
                    Toast.makeText(getApplicationContext(), "You denied write external storage permission.", Toast.LENGTH_LONG).show();
                }
            }
        }
}