package com.techsum.paysum.network;

import com.techsum.paysum.model.FeesResponse;
import com.techsum.paysum.model.LoginResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Api {

    @FormUrlEncoded
    @POST("api_login")
    Call<LoginResponse> loginResponse(
            @Field("username") String username,@Field("password") String password );


    @FormUrlEncoded
    @POST("get_fees")
    Call<FeesResponse> feesResponse(@Field("roll_no") String roll_no);

}
