package com.techsum.paysum.network;

import android.content.Context;
import android.content.SharedPreferences;

import com.techsum.paysum.model.LoginResponse;

public class SharedPrefManager {

    private static String SHARED_PREF_NAME = "tpay";
    private SharedPreferences sharedPreferences;
    Context context;
    private SharedPreferences.Editor editor;

    public SharedPrefManager( Context context) {
        this.context = context;
    }


    void saveUser(LoginResponse userResponse)
    {
        sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        editor= sharedPreferences.edit();
        editor.putString("id", userResponse.getId());
        editor.putString("school_id",userResponse.getSchoolId() );
        editor.putString("username",userResponse.getUsername());

        //editor.putString("username",userResponse.getUsername());
        //editor.putString("username",user.getUserName);
        editor.putBoolean("logged",true);
        editor.apply();
    }




    boolean isLoggedIn()
    {
        sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean("logged",false);
    }


    SharedPreferences.Editor myEdit
            = sharedPreferences.edit();



    /*public LoginResponse getUser()
    {
        sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        return new LoginResponse(sharedPreferences.getString("id",null),
                sharedPreferences.getString("school_id",null),
                sharedPreferences.getString("username",null));
    }*/



    //for user logout
    public void logout()
    {
        sharedPreferences = context.getSharedPreferences(SHARED_PREF_NAME,Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        editor.clear();
        editor.apply();
    }

}
