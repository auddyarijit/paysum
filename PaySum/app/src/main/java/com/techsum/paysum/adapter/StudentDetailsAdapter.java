package com.techsum.paysum.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.techsum.paysum.R;
import com.techsum.paysum.model.StudentFeeRsponse;
import java.util.List;

public class StudentDetailsAdapter  extends RecyclerView.Adapter<StudentDetailsAdapter.CustomViewHolder> {

    private List<StudentFeeRsponse> studentFeeRsponseList;
    private Activity activity;

    public StudentDetailsAdapter(Activity activity,List<StudentFeeRsponse> studentFeeRsponseList){
        this.activity = activity;
        this.studentFeeRsponseList = studentFeeRsponseList;
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        TextView txtTutionFee,txtAmount;

        CustomViewHolder(View itemView) {
            super(itemView);
            mView = itemView;

            txtTutionFee = mView.findViewById(R.id.txtTutionFee);
            txtAmount = mView.findViewById(R.id.txtAmount);
        }
    }

    @Override
    public StudentDetailsAdapter.CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.custom_student_detail, parent, false);
        return new StudentDetailsAdapter.CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StudentDetailsAdapter.CustomViewHolder holder, int position) {
        holder.txtTutionFee.setText(studentFeeRsponseList.get(position).getTitle());
        holder.txtAmount.setText(studentFeeRsponseList.get(position).getFeeAmount());
 /* Picasso.Builder builder = new Picasso.Builder(context);
        builder.downloader(new OkHttp3Downloader(context));
        builder.build().load(dataList.get(position).getImageurl())
                .into(holder.coverImage);*/


      //  Picasso.with(context).load(dataList.get(position).getImageurl()).into(holder.coverImage);
    }

    @Override
    public int getItemCount() {
        return studentFeeRsponseList.size();
    }
}

