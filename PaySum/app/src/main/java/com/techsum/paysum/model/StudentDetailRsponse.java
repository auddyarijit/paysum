package com.techsum.paysum.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentDetailRsponse {
    @SerializedName("roll_no")
    @Expose
    private String rollNo;
    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("student_name")
    @Expose
    private String studentName;
    @SerializedName("student_id")
    @Expose
    private String studentId;
    @SerializedName("school_id")
    @Expose
    private String schoolId;
    @SerializedName("session_year")
    @Expose
    private Object sessionYear;
    @SerializedName("class_name")
    @Expose
    private String className;

    public String getRollNo() {
        return rollNo;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getSchoolId() {
        return schoolId;
    }

    public void setSchoolId(String schoolId) {
        this.schoolId = schoolId;
    }

    public Object getSessionYear() {
        return sessionYear;
    }

    public void setSessionYear(Object sessionYear) {
        this.sessionYear = sessionYear;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

}
