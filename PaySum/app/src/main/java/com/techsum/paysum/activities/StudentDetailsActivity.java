package com.techsum.paysum.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.techsum.paysum.R;
import com.techsum.paysum.adapter.StudentDetailsAdapter;
import com.techsum.paysum.model.FeesResponse;
import com.techsum.paysum.model.StudentDetailRsponse;
import com.techsum.paysum.model.StudentFeeRsponse;
import com.techsum.paysum.network.RetrofitClient;
import com.techsum.paysum.utils.ProgressBarTransparent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by sanjay on 06/12/2020
 * module details of students
 */

public class StudentDetailsActivity extends AppCompatActivity implements View.OnClickListener{

   TextView txtMakePayment,txtActivityName;
   TextView txtStudentName,txtStudentRollNumber,txtFeeDue,txtTotalAmount;
   TextView txtGrandTotal;
   ImageView imgBack,imgLogout;
   RecyclerView recyclerView;

   //private AVLoadingIndicatorView progressBar;

   StudentDetailsAdapter studentDetailsAdapter;
   String roll_no,userId,school_id,student_id,school_name,date;
   String feeDue;

   String totalDue;
   String totalAmount;
   String title,name;
   String fullAmount;


    int amount = 0;
    int grandAmount =0;

    List<StudentDetailRsponse> studentDetailRsponseList =new ArrayList<>();
    List<StudentFeeRsponse> studentFeeRsponseList = new ArrayList<>();

    List<String>  titleList = new ArrayList<>();
    List<String>  amountList = new ArrayList<>();


    ProgressBarTransparent progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //for full screen
       /* requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/
        setContentView(R.layout.student_details);
        getSupportActionBar().hide();
        txtActivityName = findViewById(R.id.txtActivityName);
       // txtActivityName.setText("Student Details");
        txtActivityName.setText(getString(R.string.student_details));

        //Initializing Textview,CardView,ImageView
        init();

        //handle clicklistener
        clickListener();

        getData();

        imgLogout = findViewById(R.id.imgLogout);
        imgLogout.setVisibility(View.GONE);

        //for hindi translator action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));

        progressBar = ProgressBarTransparent.getInstance();

        progressBar.show(StudentDetailsActivity.this);
        studentDetailsApi();

      //  txtFeeDue.setText(feeDue);
    }


    //data come from fee activity
    private void getData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            roll_no = extras.getString("roll_no");
            userId = extras.getString("userId");
            school_id = extras.getString("school_id");
           // feeDue = extras.getString("feeDue");
        }
    }

    private void clickListener() {
        txtMakePayment.setOnClickListener(this);
        imgBack.setOnClickListener(this);
    }

    private void init() {
        txtMakePayment = findViewById(R.id.txtMakePayment);
        imgBack = findViewById(R.id.imgBack);
        txtStudentName = findViewById(R.id.txtStudentName);
        txtStudentRollNumber = findViewById(R.id.txtStudentRollNumber);
        recyclerView = findViewById(R.id.recyclerView);
        txtFeeDue = findViewById(R.id.txtFeeDue);
        txtTotalAmount = findViewById(R.id.txtTotalAmount);
        txtGrandTotal= findViewById(R.id.txtGrandTotal);
      //  progressBar = findViewById(R.id.progressBar);
    }



    private void studentDetailsApi() {

        Call<FeesResponse> call = RetrofitClient
                .getInstance()
                .getApi()
                .feesResponse(roll_no);
        call.enqueue(new Callback<FeesResponse>() {
            @Override
            public void onResponse(Call<FeesResponse> call, Response<FeesResponse> response) {

                studentDetailRsponseList = response.body().getStudent();

                for (int i=0;i<studentDetailRsponseList.size();i++)
                {
                    txtStudentName.setText(studentDetailRsponseList.get(i).getStudentName());
                    txtStudentRollNumber.setText(studentDetailRsponseList.get(i).getRollNo());
                    school_id = response.body().getStudent().get(i).getSchoolId();
                    student_id  = response.body().getStudent().get(i).getSchoolId();
                    school_name = response.body().getStudent().get(i).getClassName();

                }


                for (int i=0;i<studentDetailRsponseList.size();i++)
                {
                   name =  studentDetailRsponseList.get(i).getStudentName();
                  // roll_no =  txtStudentRollNumber.setText(studentDetailRsponseList.get(i).getRollNo());
                    school_id = response.body().getStudent().get(i).getSchoolId();
                    student_id  = response.body().getStudent().get(i).getStudentId();
                    school_name = response.body().getStudent().get(i).getSchoolName();
                }



                  studentFeeRsponseList = response.body().getFees();
                for (int i=0;i<studentFeeRsponseList.size();i++)
                {
                     totalDue = response.body().getFees().get(i).getDueAmount();
                     totalAmount = response.body().getFees().get(i).getFeeAmount();
                     amount = (amount + Integer.parseInt(totalAmount));
                }


                for (int i=0;i<studentFeeRsponseList.size();  i++)
                {
                    fullAmount = response.body().getFees().get(i).getFeeAmount();
                    amountList.add(fullAmount);

                    title = response.body().getFees().get(i).getTitle();
                    titleList.add(title);
                }



                grandAmount = amount + Integer.parseInt(totalDue);
                txtTotalAmount.setText(String.valueOf(amount));
                txtGrandTotal.setText(String.valueOf(grandAmount));

                txtFeeDue.setText(totalDue);

                detailAdapter();
                progressBar.hideProgress();
            }

            @Override
            public void onFailure(Call<FeesResponse> call, Throwable t) {
                progressBar.hideProgress();
                Toast.makeText(StudentDetailsActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void detailAdapter() {
        recyclerView = findViewById(R.id.recyclerView);
        studentDetailsAdapter = new StudentDetailsAdapter(this, studentFeeRsponseList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(studentDetailsAdapter);
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtMakePayment:
                //dailod box mode of payment
                paymentDialog();

                break;

            case R.id.imgBack:
                //finish();
                this.finish();
                Intent int1= new Intent(this, FeesActivity.class);
                int1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(int1);
                super.onBackPressed();
                break;
        }
    }


    //type of payment dailogbox
    private void paymentDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_dailog_box);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setTitle("Payment Type");
        dialog.setCancelable(false);

        TextView txtCash = dialog.findViewById(R.id.txtCash);
        TextView txtCreditUPI = dialog.findViewById(R.id.txtCreditUPI);

        //TextView for cashPaymentActivity
        txtCash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StudentDetailsActivity.this, CashPaymentActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("school_id", school_id);
                intent.putExtra("student_id", student_id);
                intent.putExtra("userId", userId);
                intent.putExtra("roll_no", roll_no);
                intent.putExtra("school_name", school_name);
                intent.putExtra("grandAmount", grandAmount);
                intent.putExtra("titleList", (Serializable) titleList);
                intent.putExtra("amountList", (Serializable) amountList);
                intent.putExtra("name", name);
                startActivity(intent);
               // startActivity(new Intent(StudentDetailsActivity.this,CashPaymentActivity.class));
                dialog.dismiss();
            }
        });


        txtCreditUPI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StudentDetailsActivity.this, SwipeCardActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("school_id", school_id);
                intent.putExtra("student_id", student_id);
                intent.putExtra("userId", userId);
                intent.putExtra("roll_no", roll_no);
                intent.putExtra("school_name", school_name);
                intent.putExtra("grandAmount", grandAmount);

                intent.putExtra("titleList", (Serializable) titleList);
                intent.putExtra("amountList", (Serializable) amountList);

                intent.putExtra("name", name);
                startActivity(intent);
            }
        });

        //ImageView use for dailog dismiss
        ImageView imgCancel = dialog.findViewById(R.id.imgCancel);
        imgCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}