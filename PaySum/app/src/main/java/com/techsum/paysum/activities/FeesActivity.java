package com.techsum.paysum.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.techsum.paysum.R;
import com.techsum.paysum.model.FeesResponse;
import com.techsum.paysum.model.StudentDetailRsponse;
import com.techsum.paysum.model.StudentFeeRsponse;
import com.techsum.paysum.network.RetrofitClient;
import com.techsum.paysum.utils.ProgressBarTransparent;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FeesActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txtStudentDetails,txtActivityName;
    ImageView imgBack,imgLogout;
    EditText edtRollNo;

    String rollno,userId,school_id;

    String feeDue;

    //private AVLoadingIndicatorView progressBar;

    List<StudentDetailRsponse> studentDetailRsponseList = new ArrayList<>();
    List<StudentFeeRsponse> feeRsponseList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fees);
        getSupportActionBar().hide();

        //set activity name on textview
        txtActivityName = findViewById(R.id.txtActivityName);
        txtActivityName.setText(getString(R.string.fee_payment));


        //Initializing Textview,CardView,ImageView
        init();
        //handle clicklistener
        clickListener();

       // imgLogout.setVisibility(View.GONE);

        //for hindi translator action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));

        getData();
    }


    private void getData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getString("userId");
            school_id = extras.getString("school_id");
           // Toast.makeText(this, ""+id +school_id, Toast.LENGTH_SHORT).show();
        }
    }



    private void init() {
        imgBack = findViewById(R.id.imgBack);
        edtRollNo = findViewById(R.id.edtRollNo);
        txtStudentDetails = findViewById(R.id.txtStudentDetails);
        //progressBar = findViewById(R.id.progressBar);
        imgLogout = findViewById(R.id.imgLogout);
        imgLogout.setVisibility(View.GONE);
    }

    private void clickListener() {
        imgBack.setOnClickListener(this);
        txtStudentDetails.setOnClickListener(this);
    }


    private void validation() {

        String roll_no = (edtRollNo.getText().toString().trim());

        if (roll_no.length()==0) {
            edtRollNo.setError(getString(R.string.roll_no_required));
            edtRollNo.requestFocus();
            return;
        }


        ProgressBarTransparent progressBar = ProgressBarTransparent.getInstance();
        progressBar.show(FeesActivity.this);

       // progressBar.show();

        Call<FeesResponse> call = RetrofitClient
                .getInstance()
                .getApi()
                .feesResponse(roll_no);

        call.enqueue(new Callback<FeesResponse>() {
            @Override
            public void onResponse(Call<FeesResponse> call, Response<FeesResponse> response) {

                  feeRsponseList =response.body().getFees();

                if (feeRsponseList.size()>0)
                {
                    studentDetailRsponseList = response.body().getStudent();
                    for (int i=0; i<studentDetailRsponseList.size(); i++)
                    {
                        rollno = studentDetailRsponseList.get(i).getRollNo();
                    }
                    Intent intent = new Intent(FeesActivity.this, StudentDetailsActivity.class);
                    /*intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);*/
                    intent.putExtra("roll_no", rollno);
                    intent.putExtra("userId", userId);
                    intent.putExtra("school_id", school_id);
                   // intent.putExtra("feeDue", feeDue);
                    startActivity(intent);
                    progressBar.hideProgress();

                }
                else {
                    progressBar.hideProgress();
                    Toast.makeText(FeesActivity.this, "Fees not Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<FeesResponse> call, Throwable t) {
                progressBar.hideProgress();
                Toast.makeText(FeesActivity.this, ""+t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onClick(View view)
    {
        switch (view.getId()) {
            case R.id.txtStudentDetails:
                validation();
                break;

            case R.id.imgBack:
                finish();
                break;
        }
    }
}