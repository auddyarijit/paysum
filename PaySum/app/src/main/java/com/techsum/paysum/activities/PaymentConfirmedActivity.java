package com.techsum.paysum.activities;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.pnsol.sdk.interfaces.DeviceType;
import com.pnsol.sdk.interfaces.ReceiptConst;
import com.pnsol.sdk.payment.PaymentInitialization;
import com.techsum.paysum.R;
import com.techsum.paysum.utils.ProgressBarTransparent;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static android.view.PixelCopy.SUCCESS;


/**
 * Created by sanjay on 06/12/2020
 * module confirm payment
 */

public class PaymentConfirmedActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgBack;
    private TextView txtPrint, txtTransactionId;
    private TextView txtName, txtRollNo, txtSchool;
    private TextView txtDate, txtTime;
    private TextView txtFeeDue, txtFeePaid, txtTotalFee;

   // private AVLoadingIndicatorView progressBar;

    int paidFee = 0;
    int dueFee = 0;


    String name, roll_no, school_name;
    int grandAmount;

    private ArrayList<String> titleList;
    private ArrayList<String> amountList;

    private Calendar calendar;
    private SimpleDateFormat dateFormat;
    private String date, time;

    private PaymentInitialization initialization;
    private Bitmap bitmap;


    //  private MaterialDialog mSimpleDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_confirmed);
        getSupportActionBar().hide();


        imgBack = findViewById(R.id.imgBack);
        txtPrint = findViewById(R.id.txtPrint);
        txtTransactionId = findViewById(R.id.txtTransactionId);
        txtName = findViewById(R.id.txtName);
        txtRollNo = findViewById(R.id.txtRollNo);
        txtSchool = findViewById(R.id.txtSchool);
       // progressBar = findViewById(R.id.progressBar);
        txtDate = findViewById(R.id.txtDate);
        txtTime = findViewById(R.id.txtTime);
        txtFeeDue = findViewById(R.id.txtFeeDue);
        txtFeePaid = findViewById(R.id.txtFeePaid);
        txtTotalFee = findViewById(R.id.txtTotalFee);
        //txtActivityName.setText("Payment Confirmed");

        imgBack.setOnClickListener(this);
        txtPrint.setOnClickListener(this);

        //for hindi translator action bar
       /* ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));*/

        getData();

        getCurrentDate();
        getCurrentTime();
    }

    private void getCurrentDate() {
        calendar = Calendar.getInstance();
        dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        date = dateFormat.format(calendar.getTime());
        txtDate.setText(date);
    }

    public void getCurrentTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("hh:mm:ss aa");
        time = mdformat.format(calendar.getTime());
        txtTime.setText(time);
    }


    private void getData() {

        ProgressBarTransparent  progressBar = ProgressBarTransparent.getInstance();
        progressBar.show(PaymentConfirmedActivity.this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            name = extras.getString("name");
            roll_no = extras.getString("roll_no");
            grandAmount = extras.getInt("grandAmount");
            school_name = extras.getString("school_name");

            paidFee = extras.getInt("paidFee");
            dueFee = extras.getInt("dueFee");

            titleList = (ArrayList<String>) getIntent().getSerializableExtra("titleList");
            amountList = (ArrayList<String>) getIntent().getSerializableExtra("amountList");
            progressBar.hideProgress();
        }

        txtName.setText(name);
        txtRollNo.setText(roll_no);
        txtTotalFee.setText(String.valueOf(grandAmount));
        txtSchool.setText(school_name);
        txtFeeDue.setText(String.valueOf(dueFee));
        txtFeePaid.setText(String.valueOf(paidFee));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgBack:
                finish();
                break;

            case R.id.txtPrint:
                printReceipt();
                break;

            default:
                break;
        }
    }

    private void printReceipt() {
        printLogo(ReceiptConst.CENTER);
        printText();
    }

    private void printText() {
        StringBuffer scriptBuffer = new StringBuffer();

        scriptBuffer.append("*text l " + "  " + " " + "Name :" + " " + name + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Roll No :" + " " + roll_no + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Transction id :" + " " + "2885385745" + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Date :" + " " + date + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Time :" + " " + time + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "School :" + " " + school_name + "\n");

        for (int i = 0; i < titleList.size(); i++) {
            scriptBuffer.append("*text l " + "  " + " " + titleList.get(i) + " - " + " " + amountList.get(i) + "\n");
        }

        scriptBuffer.append("*line" + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Amount:" + " " + grandAmount + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Fee Paid:" + " " + paidFee + "\n");
        scriptBuffer.append("*text l " + "  " + " " + "Fee Due:" + " " + dueFee + "\n");
        scriptBuffer.append("*text l " + "" + " " + " " + "\n");
        scriptBuffer.append("*text l " + "" + " " + " " + "\n");
        scriptBuffer.append("*text l " + "" + " " + " " + "\n");
        scriptBuffer.append("*text l " + "" + " " + " " + "\n");
        scriptBuffer.append("*text l " + "" + " " + " " + "\n");

        initialization = new PaymentInitialization(PaymentConfirmedActivity.this);
        initialization.printText(printerHandler, DeviceType.N910, scriptBuffer);
        paymentSuccessDialog();
    }

    private void printLogo(String alignment) {
        bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.paysum_logo);
        initialization = new PaymentInitialization(PaymentConfirmedActivity.this);
        initialization.printImage(printerHandler, DeviceType.N910, bitmap, alignment);
    }


    @SuppressLint("HandlerLeak")
    private final Handler printerHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (msg.what == SUCCESS) {
                Toast.makeText(PaymentConfirmedActivity.this,
                        (String) msg.obj, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(PaymentConfirmedActivity.this,
                        (String) msg.obj, Toast.LENGTH_SHORT).show();
            }
        }
    };




    private void paymentSuccessDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.alert_dailog_payment_success);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setTitle("Payment Type");
        dialog.setCancelable(false);

        Button btnOk = dialog.findViewById(R.id.btnOk);
        Button btnCopyReciept = dialog.findViewById(R.id.btnCopyReciept);

        //TextView for cashPaymentActivity
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentConfirmedActivity.this, DashBoardActivity.class);
                startActivity(intent);
                // startActivity(new Intent(StudentDetailsActivity.this,CashPaymentActivity.class));
                dialog.dismiss();
            }
        });


        btnCopyReciept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printLogo(ReceiptConst.CENTER);
                printText();
                Intent intent = new Intent(PaymentConfirmedActivity.this, DashBoardActivity.class);
                startActivity(intent);
                dialog.dismiss();
            }
        });


        dialog.show();
    }

}