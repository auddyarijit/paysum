package com.techsum.paysum.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FeesResponse {
    @SerializedName("fees")
    @Expose
    private List<StudentFeeRsponse> fees = null;
    @SerializedName("student")
    @Expose
    private List<StudentDetailRsponse> student = null;

    public List<StudentFeeRsponse> getFees() {
        return fees;
    }

    public void setFees(List<StudentFeeRsponse> fees) {
        this.fees = fees;
    }

    public List<StudentDetailRsponse> getStudent() {
        return student;
    }

    public void setStudent(List<StudentDetailRsponse> student) {
        this.student = student;
    }
}
