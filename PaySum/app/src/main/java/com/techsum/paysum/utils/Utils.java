package com.techsum.paysum.utils;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {


    public static String getAmountFormat(String amount) {
        StringBuilder strBind = new StringBuilder(amount);
        strBind.append(".00");
        return strBind.toString();
    }

    static boolean checkAmount(String amount) {
        try {

            if (convertStringToDouble(amount) >= 11)
                return true;
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static String getAmountFormawitdot(String amount) {
        // TODO Auto-generated method stub
        Pattern regex = Pattern.compile("[.]");
        Matcher matcher = regex.matcher(amount);
        if (matcher.find()){
            // Do something
            return amount;
        }
        else
        {
            return Utils.getAmountFormat(amount);
        }

    }

    public static double convertStringToDouble(String data) {
        try {
            NumberFormat nf = NumberFormat.getInstance(Locale.getDefault());
            double amountcheck = nf.parse(data).doubleValue();
            return amountcheck;
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0.0;
    }

    public static String getRupeeAmount(double value) {
        String rupeeAmount = "0.0";
        NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale(
                "en", "in"));
        rupeeAmount = formatter.format(value);
        return rupeeAmount;

    }

    public static String chckStringNull(String value) {
        String value1 = "";
        try {
            if (value != null && (!value.equalsIgnoreCase(null)
                    && !value.equalsIgnoreCase("null")
                    && !value.equalsIgnoreCase(""))) {
                value1 = value.trim();
            } else {
                value1 = "";
            }
        } catch (Exception e) {
            value1 = "";
        }
        return value1;
    }
}

