package com.techsum.paysum.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pnsol.sdk.interfaces.DeviceType;
import com.pnsol.sdk.payment.PaymentInitialization;
import com.pnsol.sdk.vo.response.PaymentTypes;
import com.techsum.paysum.R;
import com.techsum.paysum.utils.Utils;

import java.io.Serializable;
import java.util.ArrayList;

import static android.provider.Settings.Global.DEVICE_NAME;
import static com.pnsol.sdk.interfaces.PaymentTransactionConstants.PAYMENT_TYPE;
import static com.pnsol.sdk.interfaces.PaymentTransactionConstants.SALE;

public class SwipeCardActivity extends AppCompatActivity implements View.OnClickListener {


    private TextView txtSwipeInsert,txtActivityName;
    private ImageView imgBack,imgLogout;
    private EditText edtAmount;

    private static String DEVICE_COMMUNICATION_MODE = "transactionmode";
    private static String DEVICE_NAME = "devicename";
    private static String deviceName,deviceType,deviceCommunicationMode;
    private int deviceCommMode;


    private  TextView txtTotalDue,txtChargeRemaining;
    private String inputText ="";
    int paidFee =0;
    int dueFee =0;
    int grandAmount = 0;
    int transAmount =0;

    ArrayList<String> amountList;
    ArrayList<String> titleList;

    private String roll_no,userId,school_id,school_name,name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_card);
        getSupportActionBar().hide();


        txtSwipeInsert = findViewById(R.id.txtSwipeInsert);
        imgBack = findViewById(R.id.imgBack);
        txtActivityName = findViewById(R.id.txtActivityName);
        imgLogout= findViewById(R.id.imgLogout);
        edtAmount= findViewById(R.id.edtAmount);
        txtTotalDue= findViewById(R.id.txtTotalDue);
        txtChargeRemaining= findViewById(R.id.txtChargeRemaining);

        txtActivityName.setText("Swipe Card");

        txtSwipeInsert.setOnClickListener(this);
        imgBack.setOnClickListener(this);


        imgLogout.setVisibility(View.GONE);

        getData();
        calcutionPart();

        txtTotalDue.setText(String.valueOf(grandAmount));
    }

    private void getData() {
        Bundle bundle = getIntent().getExtras();

        if(bundle!=null)
        {
            grandAmount = bundle.getInt("grandAmount");
            school_id = bundle.getString("school_id");
            roll_no = bundle.getString("roll_no");
            school_name = bundle.getString("school_name");
            userId = bundle.getString("userId");
            grandAmount = bundle.getInt("grandAmount");
            name = bundle.getString("name");

            titleList = (ArrayList<String>) getIntent().getSerializableExtra("titleList");
            amountList = (ArrayList<String>) getIntent().getSerializableExtra("amountList");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtSwipeInsert:
                validation();
                break;

            case R.id.imgBack:
                finish();
                break;

            default:
                break;
        }
    }

    private void validation() {

        if(inputText.length()==0){
            inputText = edtAmount.getText().toString();
            edtAmount.setError(getString(R.string.pls_enter_amount));
            edtAmount.requestFocus();
            return;
        }
        cardPayment();
    }


    private void calcutionPart() {
        edtAmount.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {   //Convert the Text to String

                inputText = edtAmount.getText().toString();

                if (inputText.length()>0) {
                    paidFee = Integer.parseInt(inputText);
                    Log.d("price", String.valueOf(paidFee));
                    dueFee = (grandAmount - paidFee);
                    txtChargeRemaining.setText(Integer.toString(dueFee));
                }
                else
                {
                    txtChargeRemaining.setText(Integer.toString(grandAmount));
                }

                // for invoice transcation amount deduction
                transCalculation();

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Does not do any thing in this case
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Does not do any thing in this case
            }
        });
    }



    private void transCalculation() {
        if (paidFee>2000)
        {
            transAmount = 30;
            return;
        }
        else if(paidFee>=1000)
        {
            transAmount =20;
            return;
        }
        else if (paidFee<=1000)
        {
            transAmount = 10;
            return;
        }
    }

    private void cardPayment() {
        deviceType = DeviceType.N910;
        deviceName = DeviceType.N910;

        Intent intent = new Intent(SwipeCardActivity.this, PaymentTransactionActivity.class);
        intent.putExtra(DEVICE_NAME, deviceName);
        intent.putExtra(DEVICE_COMMUNICATION_MODE, deviceCommMode);
        intent.putExtra(PAYMENT_TYPE, "Sale");
        //intent.putExtra("referanceno", (referanceno.getText().toString()));
        intent.putExtra("amount", Utils.getAmountFormawitdot(inputText));
        intent.putExtra("grandAmount", grandAmount);
        intent.putExtra("dueFee", dueFee);
        intent.putExtra("paidFee", paidFee);
        intent.putExtra("roll_no", roll_no);
        intent.putExtra("userId", userId);
        intent.putExtra("school_id", school_id);
        intent.putExtra("school_name", school_name);
        intent.putExtra("name", name);

        intent.putExtra("transAmount", transAmount);

        intent.putExtra("titleList", (Serializable) titleList);
        intent.putExtra("amountList", (Serializable) amountList);

        //intent.putExtra("cashBackAmoumt", Utils.getAmountFormat(cashbacckamount.getText().toString()));
        startActivity(intent);
    }

}