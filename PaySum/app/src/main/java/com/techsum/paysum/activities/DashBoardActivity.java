package com.techsum.paysum.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import com.smarteist.autoimageslider.SliderView;
import com.techsum.paysum.R;
import com.techsum.paysum.adapter.SliderAdapter;
import com.techsum.paysum.model.SliderData;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.ArrayList;


/**
 * Created by sanjay on 08/12/2020
 * module DashBoard
 */

public class DashBoardActivity extends AppCompatActivity implements View.OnClickListener {

    TextView txtActivityName;
    CardView feeCardView,openSchoolCardView;
    ImageView imgBack,imgLogout;
    TextView txtTermCondition;

    String userId,school_id;

    AVLoadingIndicatorView progressBar;


    // Urls of our images.
    String url1 = "https://homepages.cae.wisc.edu/~ece533/images/airplane.png";
    String url2 = "https://qphs.fs.quoracdn.net/main-qimg-8e203d34a6a56345f86f1a92570557ba.webp";
    String url3 = "https://bizzbucket.co/wp-content/uploads/2020/08/Life-in-The-Metro-Blog-Title-22.png";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dash_board);
        getSupportActionBar().hide();

        txtActivityName = findViewById(R.id.txtActivityName);
        txtActivityName.setText(getString(R.string.dasboard));

        //Initializing Textview,CardView,ImageView
        init();

        //Click listener
        clickListener();

        //for hindi translator action bar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.app_name));
        getData();


        slider();

    }

    private void slider() {
        ArrayList<SliderData> sliderDataArrayList = new ArrayList<>();
        SliderView sliderView = findViewById(R.id.slider);

        // adding the urls inside array list
        sliderDataArrayList.add(new SliderData(url1));
        sliderDataArrayList.add(new SliderData(url2));
        sliderDataArrayList.add(new SliderData(url3));



        // passing this array list inside our adapter class.
        SliderAdapter adapter = new SliderAdapter(this, sliderDataArrayList);

        // below method is used to set auto cycle direction in left to
        // right direction you can change according to requirement.
        sliderView.setAutoCycleDirection(SliderView.LAYOUT_DIRECTION_RTL);
        // below method is used to
        // setadapter to sliderview.
        sliderView.setSliderAdapter(adapter);

        // below method is use to set
        // scroll time in seconds.
        sliderView.setScrollTimeInSec(2);

        // to set it scrollable automatically
        // we use below method.
        sliderView.setAutoCycle(true);

        // to start autocycle below method is used.
        sliderView.startAutoCycle();

    }


    private void getData() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userId = extras.getString("userId");
            school_id = extras.getString("school_id");
        }
    }



    //Initializing Textview,CardView,ImageView
    private void init() {
        feeCardView = findViewById(R.id.feeCardView);
        openSchoolCardView = findViewById(R.id.openSchoolCardView);
        imgBack = findViewById(R.id.imgBack);
        txtTermCondition = findViewById(R.id.txtTermCondition);
        imgLogout = findViewById(R.id.imgLogout);
       // progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //for backbutton hide
        imgBack.setVisibility(View.GONE);
    }


    //Click listener
    private void clickListener() {
        feeCardView.setOnClickListener(this);
        openSchoolCardView.setOnClickListener(this);
      //  imgBack.setOnClickListener(this);
        txtTermCondition.setOnClickListener(this);
        imgLogout.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.feeCardView:
                Intent intent = new Intent(DashBoardActivity.this, FeesActivity.class);
                intent.putExtra("userId", userId);
                intent.putExtra("school_id", school_id);
                startActivity(intent);
              //  startActivity(new Intent(DashBoardActivity.this, FeesActivity.class));
                break;

          /*  case R.id.imgBack:
                finish();
                break;*/

          /*  case R.id.openSchoolCardView:
                startActivity(new Intent(DashBoardActivity.this, MainActivity.class));
                break;*/

                //open website techsumitsolutions
            case R.id.txtTermCondition:
                // onPressed(txtTermCondition);
                Uri uri = Uri.parse("http://techsumitsolutions.com/");
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(i);
                break;

            case R.id.imgLogout:
                showMenuPopUp();
                break;
        }
    }





    private void showMenuPopUp() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.custom_logout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setTitle(getString(R.string.logout_msg));
        dialog.setCancelable(false);


        Button btnNo = dialog.findViewById(R.id.btnNo);
        Button btnYes = dialog.findViewById(R.id.btnYes);


        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 startActivity(new Intent(DashBoardActivity.this,LoginActivity.class));
                 dialog.dismiss();
            }
        });


        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();

      /*  PopupMenu popup = new PopupMenu(DashBoardActivity.this, imgLogout);
        popup.getMenuInflater().inflate(R.menu.menu_main, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent logout = new Intent(DashBoardActivity.this,LoginActivity.class);
                logout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(logout);
                return false;
            }
        });
        popup.show();//showing popup menu*/
    }


    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Alert !")
                .setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        DashBoardActivity.super.onBackPressed();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }

}