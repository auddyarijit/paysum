# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


#log4j2
-dontwarn org.apache.log4j.**
-dontwarn de.mindpipe.android.logging.log4j.**
-keep class org.apache.log4j.** { *;}
-keep class de.mindpipe.android.logging.log4j.** { *; }


-ignorewarnings
-dontshrink
-dontskipnonpubliclibraryclassmembers
-dontusemixedcaseclassnames
-repackageclasses ''
-allowaccessmodification
-dontpreverify

-dontwarn com.newland.**
-dontwarn com.dspread.**
-dontwarn com.nexgo.**
-dontwarn com.pnsol.sdk.**
-dontwarn org.apache.log4j.**
-dontwarn org.apache.logging.log4j.**




-optimizations !field/removal/writeonly,!field/marking/private,!class/merging/*,!code/allocation/variable

-keepattributes Exceptions,InnerClasses,Signature,Deprecated,SourceFile,LineNumberTable,LocalVariable*Table,*Annotation*,Synthetic,EnclosingMethod
-keepnames class * implements java.io.Serializable


# Also keep - Enumerations. Keep the special static methods that are required in
# enumeration classes.
-keepclassmembers enum * {
public static **[] values();
public static ** valueOf(java.lang.String);
}

-keepnames class * implements java.io.Serializable

# Also keep - Serialization code. Keep all fields and methods that are used for
# serialization.

-keepclassmembers class * implements java.io.Serializable {
static final long serialVersionUID;
private static final java.io.ObjectStreamField[] serialPersistentFields;
!static !transient <fields>;
!private <fields>;
!private <methods>;
private void writeObject(java.io.ObjectOutputStream);
private void readObject(java.io.ObjectInputStream);
java.lang.Object writeReplace();
java.lang.Object readResolve();
}


# Keep names - Native method names. Keep all native class/method names.
-keepclasseswithmembers class * {
native <methods>;
}



-keep class com.pnsol.sdk.newland.listener.** { *; }
-keep class com.pnsol.sdk.auth.** {*;}
-keep class com.pnsol.sdk.payment.PaymentInitialization {*;}
-keep class com.pnsol.sdk.payment.PaymentProcessThread {*;}
-keep class com.pnsol.sdk.payment.PaymentModeThread {*;}
-keep class com.pnsol.sdk.payment.** {*;}

-keep class com.pnsol.sdk.qpos.listener.QPOSListener {*;}
-keep class com.pnsol.sdk.payment.MiuraUpdates {*;}
-keep class com.pnsol.sdk.interfaces.** {*;}
-keep class com.pnsol.sdk.com.payswiff.km.enums.** {*;}
-keep class com.pnsol.sdk.miura.emv.tlv.ISOUtil {*;}
-keep class com.pnsol.sdk.miura.response.ResponseManager {*;}
-keep class com.pnsol.sdk.miura.messages.Message {*;}
-keep class com.pnsol.sdk.usb.USBClass{*;}
-keep class com.dspread.xpos.BLE.** { *; }
-keep class com.pnsol.sdk.util.** { *; }
-keep class com.newland.mpos.payswiff.**{*;}
-keep class org.codehaus.jackson.** { *; }
-keep class com.pnsol.sdk.n910.** { *; }
-keep class com.pnsol.sdk.n3.** { *; }
-keep class android.a.a.** { *; }
-keep class android.misc.** { *; }
-keep class android.newland.** { *; }
-keep class android.psam.** { *; }
-keep class com.a.a.** { *; }
-keep class com.newland.** { *; }
-keep class com.nlscan.android.scan.** { *; }
-keep class com.pnsol.sdk.vo.response.TransactionReportVO {*;}
-keep class com.pnsol.sdk.vo.response.TotalSummaryReportVO {*;}
-keep class com.pnsol.sdk.vo.response.SaleTrasactionReportVO {*;}
-keep class com.pnsol.sdk.vo.response.VoidTransactionReportVO {*;}
-keep class com.pnsol.sdk.nfc.T2PSDKInit {*;}
-keep class com.pnsol.sdk.nfc {*;}
-keep class com.google.android.gms.** { *; }
-keep class com.payswiff.emvlib.** { *; }
-keep class com.payswiff.attestation.** { *; }
-keep class com.payswiff.km.** { *; }
-keep class com.payswiff.t2psdk.**{*;}
-keep class com.payswiff.pst2ppaywave.**{*;}